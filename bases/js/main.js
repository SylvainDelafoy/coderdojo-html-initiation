$(function () {
  $("code.auto").each(function () {
    const content = $(this).html();
    $(this).text(content);
  });
  $(".example.auto").each(function (index) {
    const content = $(this).html();
    const targetFrame = $("<iframe class=\"result\"/>");
    function setTargetFrameContents(html) {
      targetFrame.contents().find("body").html(html);
      function updateSize(){
        targetFrame.height(targetFrame.contents().find("html").height());
      }
      updateSize();
      targetFrame.contents().find("img").on('loadend', updateSize);
      
    }
    $(this)
      .html("")
      .append(
        $("<div contenteditable='true' class=\"source\"/>")
          .text(content)
          .keyup(event => setTargetFrameContents($(event.target).text())
          )
      )
      .append(targetFrame);
    targetFrame.on('load', () => setTargetFrameContents(content))
  });
});