/*ICi, on crée des interactions avec la page. Pour plus de détails sur ce qu'il est possible de faire, il vaut mieux aller à l'atelier HTML avancé. Voici un petit exemple:*/
/*Pour eviter d'avoir à écrire beaucoup de javascript, on utilise une librairie appellée jquery (qui est chargée en premier dans le <head> de la page*/
$(function(){// Ce code est appelé une fois la page chargée. Autrement, il ne s'appliquerait qu'aux éléments déjà chargés lors de l'execution du script.
  $("h2").click(function(){// On enregistre une action lors d'un click sur les elements h2
    alert("Et voilà");//Et cette action est l'affichage d'un message. Essayez.
  });
});
