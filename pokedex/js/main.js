$(function() {
  const template = $("#template").html();
  const { forkJoin } = rxjs;
  const { concatMap, map, take, shareReplay, reduce } = rxjs.operators;
  function rawPokeData() {
    function fetchList(listEndpoint) {
      const { EMPTY } = rxjs;
      const { concatMap, expand } = rxjs.operators;
      return fetch(listEndpoint).pipe(
        expand(previous => (previous.next ? fetch(previous.next) : EMPTY), 1),
        concatMap(({ results }) => results),
        concatMap(specieShort => fetch(specieShort.url))
      );
    }
    function fetch(url) {
      const { ajax } = rxjs.ajax;
      return ajax.getJSON(url);
    }

    $types = fetchList("http://localhost:2080/api/v2/type/").pipe(
      reduce((acc, curr) => acc.set(curr.name, curr), new Map()),
      shareReplay(1)
    );

    return fetchList(`http://localhost:2080/api/v2/pokemon-species/`).pipe(
      concatMap(specie =>
        forkJoin(
          specie.varieties.map(variety =>
            fetch(variety.pokemon.url).pipe(
              concatMap(variety =>
                forkJoin(
                  variety.types.map(type =>
                    $types.pipe(
                      map(types => ({
                        ...type,
                        type: types.get(type.type.name)
                      }))
                    )
                  )
                ).pipe(map(types => ({ ...variety, types })))
              ),
              concatMap(variety =>
                forkJoin(variety.forms.map(form => fetch(form.url))).pipe(
                  map(forms => ({ ...variety, forms }))
                )
              )
            )
          )
        ).pipe(map(varieties => ({ ...specie, varieties })))
      )
    );
  }
  rawPokeData()
    .pipe(
      map(specie => {
        const { varieties } = specie;
        function forLang(lang) {
          return value => value.language.name === lang;
        }
        return {
          raw: specie,
          types: varieties[0].types.map(x =>
            x.type.names.filter(forLang("fr")).map(x => x.name)
          ),
          color: specie.color.name,
          nom: specie.names.filter(forLang("fr")).map(x => x.name),
          image: varieties[0].sprites.front_default,
          description: specie.flavor_text_entries
            .filter(forLang("fr"))
            .map(x => x.flavor_text)
        };
      })
    )
    .subscribe(
      data => {
        $("#target").append(Mustache.render(template, { pokemon: data }));
      },
      console.error,
      console.info
    );
});
